<!-- The WordPress Primary Menu -->
<?php

// display footer 1 sidebar = Footer Services
if (is_active_sidebar('custom-footer1-widget')) {
    dynamic_sidebar('custom-footer1-widget');
}

// display footer 2 sidebar = Footer Sectors
if (is_active_sidebar('custom-footer2-widget')) {
    dynamic_sidebar('custom-footer2-widget');
}

// display footer 3 sidebar = Footer menu
if (is_active_sidebar('custom-footer3-widget')) {
    dynamic_sidebar('custom-footer3-widget');
}

/*
wp_nav_menu(
    [
        'theme_location'    => 'footer',
        'menu_class'        => 'list-unstyled',
        'container_class'  => 'col-12 col-md-3',
        'container_id'    => 'navbarNav',
    ]
);

wp_nav_menu(
    [
        'theme_location'    => 'footer_2',
        'menu_class'        => 'list-unstyled',
        'container_class'  => 'col-12 col-md-5',
        'container_id'    => 'navbarNav2',
    ]
);

wp_nav_menu(
    [
        'theme_location'    => 'footer_3',
        'menu_class'        => 'list-unstyled',
        'container_class'  => 'col-12 col-md-4',
        'container_id'    => 'navbarNav3',
    ]
);
*/
?>