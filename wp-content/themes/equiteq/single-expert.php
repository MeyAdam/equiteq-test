<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);

global $post;

$profile = get_field('expert', );
$profile_image = get_field('profile_image', $post->ID);
$profile_designation = get_field('title', $post->ID);
$profile_location = get_field('location', $post->ID);
$profile_email = get_field('email', $post->ID);
$profile_contact = get_field('contact_no', $post->ID);
$profile_linkedin = esc_url(get_field('linkedin', $post->ID));
?>


<section>
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a
                href="#" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>
        <!--May implement the expert's profile here -->

        <div class="row">
            <div class="left col-md-4">
                <div class="team-bg-img">
                    <img src="<?= $profile_image['url'] ?>" alt="">
                </div>

            </div>
            <div class="right col-md-8">

                <div class="profile-title-in">
                    <h1><?= get_the_title() ?></h1>
                </div>

                <div class="py-3 profile-designation">
                    <h6><?= $profile_designation ?></h6>
                </div>

                <div class="city-title">
                    <p>
                        <i class="fa fa-map-marker fa-lg text-green" aria-hidden="true"></i>
                        <?= $profile_location->post_title ?>
                    </p>
                </div>

                <div class="social-icons">
                    <ul class="experts-socials p-0">
                        <li><a href="mailto:<?= $profile_email ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="tel:<?= $profile_contact ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                        <li><a href="<?= $profile_linkedin ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>

                <div><?= the_content() ?></div>
            </div>
        </div>

    </div>
</section>


<!--May implement the expert's industry expertise here -->

<?php
get_footer();