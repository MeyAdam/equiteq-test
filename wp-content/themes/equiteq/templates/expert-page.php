<?php
/* Template Name: Expert Page */
get_header();
$id = get_the_ID();
$page = get_post($id);

?>

<?php

/**Hero */
hm_get_template_part('template-parts/hero', ['page' => $page]);

?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <!-- <h3 class="text-uppercase mb-4">
            <?php the_title(); ?>
        </h3> -->
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php the_content(); ?>
            </div>
        </div>
        <!--May implement the search and filter here-->
    </div>
</section>

<!--May implement the experts profile list here-->

<section class="team">
    <div class="container">
        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">

            <?php
            $expert_query = new WP_Query([
                'post_type' => 'expert',
                'orderby' => 'meta_value_num',
                'meta_key' => 'expert_list_order',
                'order' => 'ASC'
            ]);

            $members = $expert_query->posts;

            foreach ($members as $member) {
                $profile_image = get_field('profile_image', $member->ID);
                $name = get_the_title($member->ID);
            }

            wp_reset_postdata();
            ?>

        </div>
    </div>
</section>

<?php
get_footer();
?>